![mail transport concept](./mail-transport.png)

# requirements

## R1: queued messages must be processed in priority order

Messages in the queue must be processed in order of their priority:
- HIGH
- MEDIUM
- LOW

## R2: messages must be sent to their destination

Messages must be forwarded to the address specified in the 'destination address'

## R3: requests must supply sufficient information

Requests must supply:
- message text
- destination address
- cryptographic function or message intention: [encrypt, sign, none]

## R4: plain text messages must not leave system boundary

Unprocessed messages must not leave the system boundary: messages that are
intended to be encrypted or signed must not leak from the system in plain text.

## R5: message requests should be processed

The system should be able to store and process an agreed number of requests
within an agreed time. Requests received over system capacity may be denied.

## additional notes

### R1

Either this should be implemented by a message queue system that support
priority ordering or by several queues. The messages should be delivered to the
nominated priority queues by an additional component behind the service
endpoint if several queues are going to be used.

### R2

The component responsible for making requests to the security service should
send transformed requests to an additional queue which will be consumed by an
entity responsible for dispatching messages to the mail server.

### R4

- it's possible that all the components will need to sit in a private subnet
  (without public route) and requests should come into a public facing LB or
  service endpoint.
- there should be some consideration given to protection of the plain text
  messages (and other attributes) between the client and the service (can use
  TLS)
- The plain text messages will need to be scrubbed from the system

# System Components

## Requests

Requests should be in some serialisable format for efficient transport. These
will need to be secured between the load balancer and the requester before
entering the system. For example, this could use TLS between the requester and
load balancer wrapping the request. There should be some consideration for
rotation of keys. This may be unnecessary if requests are bound within the same
or an additional data domain.

## Mail Transport

This represents the data and system domain. Data ingress should only be on the
Load Balancer and only egress on the dispatcher.

## Load Balancer

This optional component could manage concurrent connections to the various
queues in order to support many simultaneous requests.

## Request Queue

This is a queueing system that orders requests by priority. If such a queue is
not available then this could be achieved by a combination of queues and
additional components to perform sorting of requests. Those would then insert these either:
- into nominated priority queues (ie. having a queue for high priority requests only)
- or a secondary queue

## Security service

I'm treating this as a blackbox for now. I expect it will take plain text and
return signatures and cipher text (depending on the function). There is some
uncertainty here about key management.

## Request Consumer

This component is responsible for:
- ingesting requests from the queue
- making appropriate calls to the security service
- constructing the transformed request and preparing it for the message queue

This should have at least two threads (probably two thread pools in practice):
- one for consuming requests from the queue and making requests to the security service
- one for constructing the results of the cryptographic functions and making
  them available to the message queue

## message queue

This is responsible for holding an ordered queue of messages (including the text,
destination, and signatures). It should be consumed by the dispatcher component.

## Dispatcher

The dispatcher is responsiblge for consuming messages from the message queue
and acting as the MUA (or interfacing with an MUA) to format the messages and
signatures and submit these to the mail server.

## MSA

This is the mail server, it's responsible for actually transferring the email
to a transfer agent who queries the address if necessary and transfers to an
exchange.

# Other notes

## scalability
I'd like to consider how this system can scale for some arbitrary rate of
requests. Ideally, requests are not lost due to storage constraints in the
system. It's possible that several queueing systems can sit behind a load
balancer to serve some arbitrary number of queue consumers. For now I'm
treating the 'Security API' as a blackbox without any limit to processing but
that is likely going to be a bottleneck due to compute limits.

## keys

It's not clear currently who will hold the keys used to encrypt or sign the
messages. It's possible that the security service will provide the private part
of a key pair for signing operations. For encryption it will be more performant
to use a symmetric cipher but these keys will need to be rotated. For messages
to be recoverable after encryption those would need to be synchronised with the
recipient which seems technically challenging to automate without involving
some sort of component to manage key retrieval using some secure exchange
method.
